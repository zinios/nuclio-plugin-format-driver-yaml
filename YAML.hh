<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\format\driver\yaml
{
	use Symfony\Component\Yaml\Yaml as YAMLParser;
	use nuclio\plugin\fileSystem\reader\FileReader;
	use nuclio\plugin\format\driver\common\CommonMapInterface;

	<<provides('format::yaml')>>
	class YAML implements CommonMapInterface
	{
		/**
		 * Convert yaml/yml format string to the PHP array
		 * @param  string $file 		String that need to be converted
		 * @return Map<string,mixed>    Data that already converted.
		 */
		public function parse(string $filePath):Map<string,mixed>
		{
			$reader=FileReader::getInstance('fileSystem::local-disk',$filePath);
			$content=$reader->read();
			return new Map(YAMLParser::parse($content));
		}
		
		public function read(string $filename, Map<arraykey,mixed> $options=Map{}):Map<string,mixed>
		{
			return $this->parse($filename);
		}
		
		/**
		 * Convert PHP array format string to the yaml/yml string
		 * @param  mixed  $file 		Data that want to be converted.
		 * @return Map<string,mixed>    Yaml formatted string.
		 */
		public function dump(mixed $file):string
		{
			return YAMLParser::dump($file);
		}
		
		/**
		 * Convert PHP array format string to the yaml/yml string and write it to disk.
		 * 
		 * @param  mixed  $filename 		Data that want to be converted.
		 * @return Map<string,mixed>    Yaml formatted string.
		 */
		public function write(string $filename, Vector<mixed> $data, Map<arraykey,mixed> $options=Map{}):bool
		{
			$YAMLString=YAMLParser::dump($filename);
			return (bool)file_put_contents($filename,$YAMLString);
		}
	}
}
