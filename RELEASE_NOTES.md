Release Notes
-------------
1.1.2
-----
* Fixed use of wrong interface.

1.1.1
-----
* dump() and write() should not be static.

1.1.0
-----
* Added write method which makes this driver compatible with the new CommonInterface.

1.0.1
-----
* Fixed PSR4 path.

1.0.0
-----
* Initial Release.